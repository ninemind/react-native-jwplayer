
package com.reactlibrary;

import android.content.Intent;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReadableMap;

import javax.annotation.Nullable;

public class JWPlayerModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    private static final String PROP_LICENSE_KEY = "licenseKey";
    private static final String PROP_FILE = "file";

    public JWPlayerModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "JWPlayer";
    }

    @ReactMethod
    public void playVideo(@Nullable ReadableMap video) {
        Intent intent = new Intent(this.reactContext, JWPlayerActivity.class);

        final String licenseKey = video.getString(PROP_LICENSE_KEY);
        final String file = video.getString(PROP_FILE);

        intent.putExtra(PROP_LICENSE_KEY, licenseKey);
        intent.putExtra(PROP_FILE, file);

        this.reactContext.startActivity(intent);
    }

}
