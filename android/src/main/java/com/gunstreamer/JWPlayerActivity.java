
package com.reactlibrary;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.longtailvideo.jwplayer.JWPlayerFragment;
import com.longtailvideo.jwplayer.JWPlayerView;
import com.longtailvideo.jwplayer.configuration.PlayerConfig;


public class JWPlayerActivity extends Activity {

    private static final String PROP_LICENSE_KEY = "licenseKey";
    private static final String PROP_FILE = "file";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_jwplayer);

        Bundle extras = getIntent().getExtras();
        final String licenseKey = extras.getString(PROP_LICENSE_KEY);
        final String file = extras.getString(PROP_FILE);

        JWPlayerView.setLicenseKey(getBaseContext(), licenseKey);

        PlayerConfig playerConfig = new PlayerConfig.Builder()
            .file(file)
            .build();

        JWPlayerFragment fragment = JWPlayerFragment.newInstance(playerConfig);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.fragmentContainer, fragment);
        ft.commit();
    }

}
