
import React, { Component, PropTypes } from 'react';
import { NativeModules, View } from 'react-native';

class JWPlayer extends Component {
  render() {
    <View>
      <RTCJWPlayer
        licenseKey={this.props.licenseKey}
      />
    </View>
  }
}

module.exports = requireNativeComponent('RTCJWPlayer', JWPlayer);
